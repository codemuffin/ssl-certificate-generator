@echo off

xcopy output\server.crt C:\xampp\apache\conf\ssl.crt /y
xcopy output\server.key C:\xampp\apache\conf\ssl.key /y

echo Copied cert and key to: C:\xampp\apache\conf

pause