# HTTPS Certificate Generator

Self-signed certificate generator.

This lets you to browse local sites that would otherwise be flagged as insecure by Chrome (which marks them with `ERR_CERT_COMMON_NAME_INVALID`).

## Usage

1. Edit CUSTOM-dns.ext and add your local sites
1. Generate certs with generate.bat
1. Install with install.bat. If a certificate already exists with the same name, it will not be replaced, and you will need to perform a manual installation.

## XAMPP

You can copy to your XAMPP dir with the provided script.

It presumes your XAMPP install is here:

    c:\xampp\

And your vhosts point to these cert/key locations:

    SSLCertificateFile "conf/ssl.crt/server.crt"
    SSLCertificateKeyFile "conf/ssl.key/server.key"

## IIS

There is also a PFX cert script in the output folder for use with local IIS projects.

## Manual Install

If you don't want to use install.bat, you can install your custom cert manually:

- Open `output\server.crt` and click "Install Certificate..."
  - Note that the icon shows a red X. This indicates that it's not been installed yet.
- Leave the choice of "Current User", and click Next
- Choose the second option, "Place all certificates...", and click Browse
- Click the "Trusted Root Certification Authorities", then click OK
- Click Next, then Finish, then Yes in the dialog popup, then OK in the next dialog
- Close the certificate by clicking OK
- Re-open the certificate. The red X should no longer be present

### Uninstall

To remove the certificate, open Manage Certs.msc and manually delete the custom certificate. You can open `output\server.crt` to find its name, under "Issued to".

Chrome won't forget the certificate until you restart it, but you can quickly test the certificate's removal with an Incognito tab.

## Troubleshooting

The security status icon can help diagnose the issue. It's shown in the address bar, to the left of the URL:

  - 🔒 – Everything is fine!
  - ℹ – Something is insecure
  - ⚠ – Certificate is invalid (always shows when you bypass the "This site is insecure" warning)

Please try the following things:

  1. If you were running XAMPP or BrowserSync, restart them.
  1. If you're using BrowserSync, make sure you specified the custom SSL cert (its default one is called "Internet Widgits").
  1. Make sure you're bypassing any service workers: In Chrome's developer tools, click Application, then Service Workers. Check the box labelled "Bypass for network".
  1. Check the console for any "Mixed Content" warnings (eg, serving an image over http instead or https).
  1. Check the certificate the browser is using:
    - Click the security status icon in the address bar, then click Certificate
    - If the certificate shown says "Issued by: XAMPP - Code Muffin", the correct certificate is being used.
    - If the certificate icon shows a red X, and the text in the General tab says "This CA Root certificate is not trusted", then it wasn't installed correctly. Check your installed certificates in Internet Options (see Uninstall).
  1. Restart Chrome fully, either with the provided shortcut, or by entering the following in the address bar: `chrome://restart`
  1. Reset the content permissions for the page: Click (i) > Site Settings > Reset Permissions. Then restart Chrome fully.
  1. In Internet Options, under Content, click Clear SSL State. This will delete cached certificates (don't worry, it will not delete anything).
  1. Use Incognito mode. This should always show the currently used certificate.
