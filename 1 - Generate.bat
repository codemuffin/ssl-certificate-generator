@echo off

set OPENSSL_CONF=./config.cnf

if not exist .\output mkdir .\output

openssl req -new -out server.csr -passin pass:password -passout pass:password -batch
openssl rsa -in privkey.pem -out server.key -passin pass:password
openssl x509 -in server.csr -out server.crt -req -signkey server.key -days 3650 -sha256 -extfile CUSTOM-dns.ext

set OPENSSL_CONF=
del .rnd
del privkey.pem
del server.csr

move /y server.crt .\output
move /y server.key .\output

echo Done
pause
